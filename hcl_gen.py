import numpy as np
import os
from keras import backend as K
from keras.utils import Sequence, to_categorical

# Here, `x_set` is list of path to the images
# and `y_set` are the associated classes.

class HCLSequence(Sequence):
    #CHAR_PER_FILE = 3755 # hh and xx files
    #CHAR_PER_FILE = 1000 # ch files

    def __init__(self, file_path, batch_size, start_idx=0, chunk=(0,3755)):
        self.file_list=[x for x in sorted(os.listdir(file_path))]
        self.batch_size = batch_size
        self.path = file_path
        self.start_idx = start_idx
        self.begin_char = chunk[0]
        self.CHAR_PER_FILE = chunk[1]


    def __len__(self):
        return int(np.ceil(len(self.file_list)*self.CHAR_PER_FILE / float(self.batch_size))) - self.start_idx

    def read_at_offset(self,filename,offset, read_count):
        X=[]
        file_to_open= "{}/{}".format(self.path,filename)
        #print(file_to_open, offset, read_count)
        with open(file_to_open, "rb") as f:
            meta = f.read(512) # read first block, it is meta data
            s= np.zeros((64,64))
            f.seek(512*(offset+1+self.begin_char))
            for c in range(read_count):
                sx = f.read(512)
                # Do stuff with byte.
                for i in range(64):
                    for k in range(8):
                        dat=sx[i*8+k]
                        for j in reversed(range(8)):
                            s[i][k*8+7-j]=(dat >> j)&0x01
                X.append(np.copy(s))
        return X
    def __getitem__(self, idx):
        X=[]
        idx+=self.start_idx
        offset=(idx*self.batch_size) % self.CHAR_PER_FILE
        fid=idx*self.batch_size//self.CHAR_PER_FILE
        #print(offset,fid, self.file_list[fid])

        if self.CHAR_PER_FILE - offset < self.batch_size: #need read next file 
            X+=self.read_at_offset(self.file_list[fid], offset, self.CHAR_PER_FILE - offset ) # read till the end

            X+=self.read_at_offset(self.file_list[(fid+1)%len(self.file_list)], 0, self.batch_size-self.CHAR_PER_FILE + offset) # read the left char from next file
        else:
            X+=self.read_at_offset(self.file_list[fid], offset, self.batch_size)
        X_raw=np.array(X)
        y_raw=np.array([(offset+i)%self.CHAR_PER_FILE for i in range(self.batch_size)])+self.begin_char
        if K.image_data_format() == 'channels_first':
            X_raw = X_raw.reshape(X_raw.shape[0], 1, 64, 64)
        else:
            X_raw = X_raw.reshape(X_raw.shape[0], 64, 64, 1)
        return X_raw,to_categorical(y_raw,3755)
