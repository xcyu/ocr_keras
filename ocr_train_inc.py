from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.losses import categorical_crossentropy
from keras.optimizers import Adadelta
from keras.utils import multi_gpu_model
from hcl_gen import HCLSequence
from keras import backend as K
import tensorflow as tf
import json
from keras.callbacks import ModelCheckpoint, EarlyStopping

# Basic model info / hyperparams
batch_size = 2048
num_category = 3755
epochs = 12

train_path = 'OCR/train'
test_path = 'OCR/test'
# input image dimensions

img_rows, img_cols = 64, 64
if K.image_data_format() == 'channels_first':
    input_shape = (1, img_rows, img_cols)
else:
    input_shape = (img_rows, img_cols, 1)
myGenTrainLen=len(HCLSequence(train_path, batch_size)) # 700
myGenTest= HCLSequence(test_path, batch_size,len(HCLSequence(test_path, batch_size))//3) # 300 hack: take only 2 thirds from 300 - start from first third

## callbacks: 1. early stopping, 2. save best
callbacks = [
    EarlyStopping(
        monitor='val_acc',
        patience=10,
        mode='max',
        verbose=1),
    ModelCheckpoint("checkpoint.{epoch:02d}.hdf5",
        monitor='val_acc',
        save_best_only=True,
        mode='max',
        verbose=0)
]


##model building
with tf.device('/cpu:0'):
    model = Sequential()
#convolutional layer with rectified linear unit activation
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
#32 convolution filters used each of size 3x3
#again
model.add(Conv2D(64, (3, 3), activation='relu'))
#64 convolution filters used each of size 3x3
#choose the best features via pooling
model.add(MaxPooling2D(pool_size=(2, 2)))
#randomly turn neurons on and off to improve convergence
model.add(Dropout(0.25))
#flatten since too many dimensions, we only want a classification output
model.add(Flatten())
#fully connected to get all relevant data
model.add(Dense(128, activation='relu'))
#one more dropout for convergence' sake :)
model.add(Dropout(0.5))
#output a softmax to squash the matrix into output probabilities
model.add(Dense(num_category, activation='softmax'))
## generate multi_gpu_model from base template
parallel_model = multi_gpu_model(model,gpus=2)
#parallel_model = model
parallel_model.compile(loss=categorical_crossentropy,
              optimizer=Adadelta(),
              metrics=['accuracy'])
stepSize= int(100*num_category//batch_size)
for start in range(0,myGenTrainLen ,stepSize):
    hist=parallel_model.fit_generator(HCLSequence(train_path,batch_size,start), steps_per_epoch=stepSize, epochs=epochs, verbose=1,  validation_data=myGenTest, max_queue_size=10, workers=8, use_multiprocessing=True, shuffle=False, initial_epoch=0, callbacks=callbacks)
## evaluate and generate score:
    score=parallel_model.evaluate_generator(myGenTest, 10, workers=10, use_multiprocessing=True, verbose=1)
    print('Inc:', start, ' Test score:', score[0])
    print('Inc:', start, ' Test accuracy:', score[1])
    model.save("ocr_model_ep{}_ba{}_inc{}.h5".format(epochs,batch_size,start))
    with open("ocr_hist_ep{}_ba{}_inc{}.json".format(epochs,batch_size,start), 'w') as f:
        json.dump(hist.history, f)
