root@0d61a129c7ef:/notebooks/hill# python ocr_train_inc_prio.py
Using TensorFlow backend.
2018-12-20 06:14:17.944623: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2018-12-20 06:14:19.154397: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:964] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2018-12-20 06:14:19.154825: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1432] Found device 0 with properties:
name: GeForce GTX 1080 Ti major: 6 minor: 1 memoryClockRate(GHz): 1.6325
pciBusID: 0000:01:00.0
totalMemory: 10.92GiB freeMemory: 10.76GiB
2018-12-20 06:14:19.230725: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:964] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2018-12-20 06:14:19.234252: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1432] Found device 1 with properties:
name: GeForce GTX 1080 Ti major: 6 minor: 1 memoryClockRate(GHz): 1.6325
pciBusID: 0000:02:00.0
totalMemory: 10.92GiB freeMemory: 10.76GiB
2018-12-20 06:14:19.239598: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1511] Adding visible gpu devices: 0, 1
2018-12-20 06:14:19.902034: I tensorflow/core/common_runtime/gpu/gpu_device.cc:982] Device interconnect StreamExecutor with strength 1 edge matrix:
2018-12-20 06:14:19.902067: I tensorflow/core/common_runtime/gpu/gpu_device.cc:988]      0 1
2018-12-20 06:14:19.902074: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1001] 0:   N Y
2018-12-20 06:14:19.902078: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1001] 1:   Y N
2018-12-20 06:14:19.902349: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1115] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 10404 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1080 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1)
2018-12-20 06:14:19.905306: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1115] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:1 with 10405 MB memory) -> physical GPU (device: 1, name: GeForce GTX 1080 Ti, pci bus id: 0000:02:00.0, compute capability: 6.1)
Epoch 1/12
183/183 [==============================] - 330s 2s/step - loss: 8.0321 - acc: 0.0017 - val_loss: 6.7555 - val_acc: 0.0228
Epoch 2/12
183/183 [==============================] - 331s 2s/step - loss: 6.8153 - acc: 0.0182 - val_loss: 5.6545 - val_acc: 0.0953
Epoch 3/12
183/183 [==============================] - 332s 2s/step - loss: 5.6054 - acc: 0.0635 - val_loss: 3.8986 - val_acc: 0.3204
Epoch 4/12
183/183 [==============================] - 332s 2s/step - loss: 4.6577 - acc: 0.1383 - val_loss: 3.0200 - val_acc: 0.4476
Epoch 5/12
183/183 [==============================] - 331s 2s/step - loss: 4.2641 - acc: 0.1942 - val_loss: 2.3969 - val_acc: 0.5862
Epoch 6/12
183/183 [==============================] - 330s 2s/step - loss: 3.9101 - acc: 0.2470 - val_loss: 1.8635 - val_acc: 0.6553
Epoch 7/12
183/183 [==============================] - 328s 2s/step - loss: 2.9145 - acc: 0.3618 - val_loss: 1.6436 - val_acc: 0.6783
Epoch 8/12
183/183 [==============================] - 336s 2s/step - loss: 2.6641 - acc: 0.4193 - val_loss: 1.4968 - val_acc: 0.7114
Epoch 9/12
183/183 [==============================] - 333s 2s/step - loss: 2.5493 - acc: 0.4479 - val_loss: 1.3308 - val_acc: 0.7277
Epoch 10/12
183/183 [==============================] - 331s 2s/step - loss: 2.2634 - acc: 0.4909 - val_loss: 1.3196 - val_acc: 0.7323
Epoch 11/12
183/183 [==============================] - 330s 2s/step - loss: 2.2163 - acc: 0.5028 - val_loss: 1.1263 - val_acc: 0.7671
Epoch 12/12
183/183 [==============================] - 329s 2s/step - loss: 2.4481 - acc: 0.4748 - val_loss: 1.1865 - val_acc: 0.7812
10/10 [==============================] - 12s 1s/step
Inc: 0  Test score: 0.8284424245357513
Inc: 0  Test accuracy: 0.845703125
Epoch 1/12
366/366 [==============================] - 455s 1s/step - loss: 1.9764 - acc: 0.5499 - val_loss: 1.0680 - val_acc: 0.7705
Epoch 2/12
366/366 [==============================] - 442s 1s/step - loss: 1.8353 - acc: 0.5735 - val_loss: 0.8713 - val_acc: 0.8132
Epoch 3/12
366/366 [==============================] - 441s 1s/step - loss: 2.1983 - acc: 0.5188 - val_loss: 0.9212 - val_acc: 0.8044
Epoch 4/12
366/366 [==============================] - 444s 1s/step - loss: 1.6701 - acc: 0.6031 - val_loss: 0.8780 - val_acc: 0.8140
Epoch 5/12
366/366 [==============================] - 443s 1s/step - loss: 1.6318 - acc: 0.6150 - val_loss: 0.8773 - val_acc: 0.8065
Epoch 6/12
366/366 [==============================] - 443s 1s/step - loss: 1.7512 - acc: 0.5965 - val_loss: 0.8286 - val_acc: 0.8339
Epoch 7/12
366/366 [==============================] - 441s 1s/step - loss: 1.7561 - acc: 0.5923 - val_loss: 0.7481 - val_acc: 0.8336
Epoch 8/12
366/366 [==============================] - 444s 1s/step - loss: 1.5021 - acc: 0.6448 - val_loss: 0.7472 - val_acc: 0.8377
Epoch 9/12
366/366 [==============================] - 442s 1s/step - loss: 1.4478 - acc: 0.6511 - val_loss: 0.8181 - val_acc: 0.8285
Epoch 10/12
366/366 [==============================] - 442s 1s/step - loss: 1.7935 - acc: 0.5928 - val_loss: 0.7061 - val_acc: 0.8486
Epoch 11/12
366/366 [==============================] - 445s 1s/step - loss: 1.3727 - acc: 0.6650 - val_loss: 0.6552 - val_acc: 0.8509
Epoch 12/12
366/366 [==============================] - 443s 1s/step - loss: 1.3786 - acc: 0.6671 - val_loss: 0.6818 - val_acc: 0.8432
10/10 [==============================] - 12s 1s/step
Inc: 183  Test score: 0.43542220816016197
Inc: 183  Test accuracy: 0.888427734375
Epoch 1/12
549/549 [==============================] - 567s 1s/step - loss: 1.2968 - acc: 0.6857 - val_loss: 0.6814 - val_acc: 0.8477
Epoch 2/12
549/549 [==============================] - 553s 1s/step - loss: 1.5754 - acc: 0.6339 - val_loss: 0.6538 - val_acc: 0.8567
Epoch 3/12
549/549 [==============================] - 555s 1s/step - loss: 1.2597 - acc: 0.6915 - val_loss: 0.6419 - val_acc: 0.8521
Epoch 4/12
549/549 [==============================] - 555s 1s/step - loss: 1.3441 - acc: 0.6764 - val_loss: 0.6406 - val_acc: 0.8661
Epoch 5/12
549/549 [==============================] - 555s 1s/step - loss: 1.3565 - acc: 0.6750 - val_loss: 0.6665 - val_acc: 0.8492
Epoch 6/12
549/549 [==============================] - 555s 1s/step - loss: 1.2142 - acc: 0.7008 - val_loss: 0.6307 - val_acc: 0.8623
Epoch 7/12
549/549 [==============================] - 552s 1s/step - loss: 1.4201 - acc: 0.6605 - val_loss: 0.5822 - val_acc: 0.8663
Epoch 8/12
549/549 [==============================] - 556s 1s/step - loss: 1.1587 - acc: 0.7152 - val_loss: 0.5694 - val_acc: 0.8663
Epoch 9/12
549/549 [==============================] - 553s 1s/step - loss: 1.3854 - acc: 0.6701 - val_loss: 0.5633 - val_acc: 0.8745
Epoch 10/12
549/549 [==============================] - 556s 1s/step - loss: 1.1481 - acc: 0.7150 - val_loss: 0.5463 - val_acc: 0.8763
Epoch 11/12
549/549 [==============================] - 554s 1s/step - loss: 1.2007 - acc: 0.7032 - val_loss: 0.5611 - val_acc: 0.8730
Epoch 12/12
549/549 [==============================] - 556s 1s/step - loss: 1.2660 - acc: 0.6939 - val_loss: 0.5334 - val_acc: 0.8755
10/10 [==============================] - 8s 797ms/step
Inc: 366  Test score: 0.3104525201022625
Inc: 366  Test accuracy: 0.920361328125
Epoch 1/12
732/732 [==============================] - 675s 922ms/step - loss: 1.1017 - acc: 0.7265 - val_loss: 0.5087 - val_acc: 0.8823
Epoch 2/12
732/732 [==============================] - 665s 908ms/step - loss: 1.2549 - acc: 0.6954 - val_loss: 0.5395 - val_acc: 0.8773
Epoch 3/12
732/732 [==============================] - 664s 907ms/step - loss: 1.1567 - acc: 0.7138 - val_loss: 0.5480 - val_acc: 0.8816
Epoch 4/12
732/732 [==============================] - 666s 909ms/step - loss: 1.1644 - acc: 0.7132 - val_loss: 0.5110 - val_acc: 0.8821
Epoch 5/12
732/732 [==============================] - 664s 907ms/step - loss: 1.2141 - acc: 0.7028 - val_loss: 0.5060 - val_acc: 0.8844
Epoch 6/12
732/732 [==============================] - 665s 908ms/step - loss: 1.0438 - acc: 0.7366 - val_loss: 0.4976 - val_acc: 0.8820
Epoch 7/12
732/732 [==============================] - 663s 906ms/step - loss: 1.2066 - acc: 0.7024 - val_loss: 0.5251 - val_acc: 0.8777
Epoch 8/12
732/732 [==============================] - 665s 908ms/step - loss: 1.0292 - acc: 0.7411 - val_loss: 0.4867 - val_acc: 0.8862
Epoch 9/12
732/732 [==============================] - 671s 917ms/step - loss: 1.1766 - acc: 0.7106 - val_loss: 0.4844 - val_acc: 0.8853
Epoch 10/12
732/732 [==============================] - 673s 919ms/step - loss: 1.0697 - acc: 0.7303 - val_loss: 0.4798 - val_acc: 0.8895
Epoch 11/12
732/732 [==============================] - 678s 926ms/step - loss: 1.1187 - acc: 0.7227 - val_loss: 0.4901 - val_acc: 0.8828
Epoch 12/12
732/732 [==============================] - 679s 928ms/step - loss: 1.1422 - acc: 0.7162 - val_loss: 0.5079 - val_acc: 0.8865
10/10 [==============================] - 8s 841ms/step
Inc: 549  Test score: 0.35204270482063293
Inc: 549  Test accuracy: 0.913330078125
Epoch 1/12
915/915 [==============================] - 806s 881ms/step - loss: 1.0480 - acc: 0.7366 - val_loss: 0.4926 - val_acc: 0.8885
Epoch 2/12
915/915 [==============================] - 799s 874ms/step - loss: 1.0583 - acc: 0.7335 - val_loss: 0.4890 - val_acc: 0.8836
Epoch 3/12
915/915 [==============================] - 801s 875ms/step - loss: 1.1063 - acc: 0.7239 - val_loss: 0.4951 - val_acc: 0.8843
Epoch 4/12
915/915 [==============================] - 799s 874ms/step - loss: 1.0939 - acc: 0.7255 - val_loss: 0.4607 - val_acc: 0.8934
Epoch 5/12
915/915 [==============================] - 804s 879ms/step - loss: 0.9768 - acc: 0.7502 - val_loss: 0.5008 - val_acc: 0.8791
Epoch 6/12
915/915 [==============================] - 805s 880ms/step - loss: 1.0858 - acc: 0.7284 - val_loss: 0.4474 - val_acc: 0.8948
Epoch 7/12
915/915 [==============================] - 801s 876ms/step - loss: 1.0780 - acc: 0.7274 - val_loss: 0.4593 - val_acc: 0.8928
Epoch 8/12
915/915 [==============================] - 805s 880ms/step - loss: 0.9911 - acc: 0.7476 - val_loss: 0.4518 - val_acc: 0.8950
Epoch 9/12
915/915 [==============================] - 807s 881ms/step - loss: 1.0297 - acc: 0.7392 - val_loss: 0.4530 - val_acc: 0.8919
Epoch 10/12
915/915 [==============================] - 806s 881ms/step - loss: 1.0603 - acc: 0.7329 - val_loss: 0.4454 - val_acc: 0.8936
Epoch 11/12
915/915 [==============================] - 806s 881ms/step - loss: 1.0448 - acc: 0.7354 - val_loss: 0.4601 - val_acc: 0.8931
Epoch 12/12
915/915 [==============================] - 808s 883ms/step - loss: 0.9492 - acc: 0.7555 - val_loss: 0.4375 - val_acc: 0.8972
10/10 [==============================] - 13s 1s/step
Inc: 732  Test score: 0.27543911933898924
Inc: 732  Test accuracy: 0.92880859375
Epoch 1/12
1098/1098 [==============================] - 933s 850ms/step - loss: 1.0264 - acc: 0.7412 - val_loss: 0.4560 - val_acc: 0.8951
Epoch 2/12
1098/1098 [==============================] - 924s 842ms/step - loss: 0.9679 - acc: 0.7515 - val_loss: 0.4527 - val_acc: 0.8973
Epoch 3/12
1098/1098 [==============================] - 925s 843ms/step - loss: 0.9804 - acc: 0.7488 - val_loss: 0.4679 - val_acc: 0.8906
Epoch 4/12
1098/1098 [==============================] - 926s 843ms/step - loss: 1.0113 - acc: 0.7426 - val_loss: 0.4313 - val_acc: 0.8954
Epoch 5/12
1098/1098 [==============================] - 925s 843ms/step - loss: 1.0143 - acc: 0.7421 - val_loss: 0.4253 - val_acc: 0.8991
Epoch 6/12
1098/1098 [==============================] - 927s 844ms/step - loss: 1.0048 - acc: 0.7440 - val_loss: 0.4248 - val_acc: 0.8976
Epoch 7/12
1098/1098 [==============================] - 924s 841ms/step - loss: 1.0013 - acc: 0.7430 - val_loss: 0.4267 - val_acc: 0.8981
Epoch 8/12
1098/1098 [==============================] - 926s 843ms/step - loss: 0.9858 - acc: 0.7487 - val_loss: 0.4492 - val_acc: 0.8967
Epoch 9/12
1098/1098 [==============================] - 928s 845ms/step - loss: 0.9368 - acc: 0.7576 - val_loss: 0.4424 - val_acc: 0.8976
Epoch 10/12
1098/1098 [==============================] - 929s 846ms/step - loss: 0.9571 - acc: 0.7532 - val_loss: 0.4221 - val_acc: 0.9005
Epoch 11/12
1098/1098 [==============================] - 928s 845ms/step - loss: 0.9897 - acc: 0.7469 - val_loss: 0.4180 - val_acc: 0.8993
Epoch 12/12
1098/1098 [==============================] - 930s 847ms/step - loss: 0.9890 - acc: 0.7468 - val_loss: 0.4335 - val_acc: 0.8986
10/10 [==============================] - 9s 926ms/step
Inc: 915  Test score: 0.2598806083202362
Inc: 915  Test accuracy: 0.934033203125
Epoch 1/12
1281/1281 [==============================] - 1052s 821ms/step - loss: 0.9638 - acc: 0.7519 - val_loss: 0.4056 - val_acc: 0.9028
Epoch 2/12
1281/1281 [==============================] - 1046s 817ms/step - loss: 0.9630 - acc: 0.7518 - val_loss: 0.4121 - val_acc: 0.9008
Epoch 3/12
1281/1281 [==============================] - 1046s 816ms/step - loss: 0.9580 - acc: 0.7530 - val_loss: 0.4256 - val_acc: 0.8975
Epoch 4/12
1281/1281 [==============================] - 1047s 817ms/step - loss: 0.9540 - acc: 0.7537 - val_loss: 0.4416 - val_acc: 0.8944
Epoch 5/12
1281/1281 [==============================] - 1048s 818ms/step - loss: 0.9486 - acc: 0.7548 - val_loss: 0.4130 - val_acc: 0.9014
Epoch 6/12
1281/1281 [==============================] - 1047s 817ms/step - loss: 0.9483 - acc: 0.7545 - val_loss: 0.4112 - val_acc: 0.9013
Epoch 7/12
1281/1281 [==============================] - 1048s 818ms/step - loss: 0.9421 - acc: 0.7559 - val_loss: 0.4045 - val_acc: 0.9029
Epoch 8/12
1281/1281 [==============================] - 1047s 817ms/step - loss: 0.9408 - acc: 0.7565 - val_loss: 0.4052 - val_acc: 0.9027
Epoch 9/12
1281/1281 [==============================] - 1050s 819ms/step - loss: 0.9399 - acc: 0.7563 - val_loss: 0.4175 - val_acc: 0.9015
Epoch 10/12
1281/1281 [==============================] - 1050s 820ms/step - loss: 0.9351 - acc: 0.7573 - val_loss: 0.4923 - val_acc: 0.8817
Epoch 11/12
1281/1281 [==============================] - 1051s 820ms/step - loss: 0.9303 - acc: 0.7583 - val_loss: 0.4380 - val_acc: 0.8940
Epoch 12/12
1281/1281 [==============================] - 1051s 820ms/step - loss: 0.9311 - acc: 0.7581 - val_loss: 0.4064 - val_acc: 0.9016
10/10 [==============================] - 9s 939ms/step
Inc: 1098  Test score: 0.2595333158969879
Inc: 1098  Test accuracy: 0.930908203125
Epoch 1/12
1464/1464 [==============================] - 1181s 807ms/step - loss: 0.9195 - acc: 0.7618 - val_loss: 0.4176 - val_acc: 0.9000
Epoch 2/12
1464/1464 [==============================] - 1166s 796ms/step - loss: 0.9168 - acc: 0.7612 - val_loss: 0.4025 - val_acc: 0.9025
Epoch 3/12
1464/1464 [==============================] - 1168s 798ms/step - loss: 0.9098 - acc: 0.7623 - val_loss: 0.4037 - val_acc: 0.9009
Epoch 4/12
1464/1464 [==============================] - 1156s 790ms/step - loss: 0.9117 - acc: 0.7617 - val_loss: 0.3968 - val_acc: 0.9044
Epoch 5/12
1464/1464 [==============================] - 1117s 763ms/step - loss: 0.9273 - acc: 0.7583 - val_loss: 0.3985 - val_acc: 0.9050
Epoch 6/12
1464/1464 [==============================] - 1112s 759ms/step - loss: 0.9405 - acc: 0.7556 - val_loss: 0.4072 - val_acc: 0.9044
Epoch 7/12
1464/1464 [==============================] - 1111s 759ms/step - loss: 0.9114 - acc: 0.7613 - val_loss: 0.3944 - val_acc: 0.9047
Epoch 8/12
1464/1464 [==============================] - 1111s 759ms/step - loss: 0.8980 - acc: 0.7658 - val_loss: 0.4006 - val_acc: 0.9036
Epoch 9/12
1464/1464 [==============================] - 1111s 759ms/step - loss: 0.9024 - acc: 0.7636 - val_loss: 0.4032 - val_acc: 0.9042
Epoch 10/12
1464/1464 [==============================] - 1112s 760ms/step - loss: 0.8937 - acc: 0.7655 - val_loss: 0.3959 - val_acc: 0.9036
Epoch 11/12
1464/1464 [==============================] - 1109s 758ms/step - loss: 0.8905 - acc: 0.7660 - val_loss: 0.4175 - val_acc: 0.8998
Epoch 12/12
1464/1464 [==============================] - 1110s 758ms/step - loss: 0.9048 - acc: 0.7629 - val_loss: 0.4025 - val_acc: 0.9051
10/10 [==============================] - 12s 1s/step
Inc: 1281  Test score: 0.24660147838294505
Inc: 1281  Test accuracy: 0.9369140625
